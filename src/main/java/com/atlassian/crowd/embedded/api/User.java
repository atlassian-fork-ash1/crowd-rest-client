/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.embedded.api;

import java.security.Principal;

/**
 * Represents a user.
 */
public interface User extends Comparable<User>, Principal
{
    /**
     * @return id of the directory in which the User is stored.
     */
    long getDirectoryId();

    /**
     * @return <code>true</code> if and only if the user is allowed to authenticate.
     */
    boolean isActive();

    /**
     * @return email address of the user.
     */
    String getEmailAddress();

    /**
     * @return display name (eg. full name) of the user, must never be null.
     */
    String getDisplayName();

    /**
     * Implementations must ensure equality based on
     * getDirectoryId() and case-insensitive getName().
     *
     * @param o object to compare to.
     * @return <code>true</code> if and only if the directoryId
     *         and name.toLowerCase() of the directory entities match.
     */
    boolean equals(Object o);

    /**
     * Implementations must produce a hashcode based on
     * getDirectoryId() and case-insensitive getName().
     *
     * @return hashcode.
     */
    int hashCode();

    /**
     * CompareTo must be compatible with the equals() and hashCode() methods
     */
    int compareTo(User user);
}
