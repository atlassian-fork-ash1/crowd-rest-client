/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.exception;

import org.apache.commons.lang3.StringEscapeUtils;

/**
 * Thrown when the attempted authentication is not valid.
 */
public class InvalidAuthenticationException extends CrowdException
{
    /**
     * Constructs a new <code>InvalidAuthenticationException</code> with the specified detail message.
     *
     * @param msg detail message
     */
    public InvalidAuthenticationException(final String msg)
    {
        super(msg);
    }

    /**
     * Constructs a new <code>InvalidAuthenticationException</code> with the specified detail message and cause.
     *
     * @param msg detail message
     * @param cause the cause
     */
    public InvalidAuthenticationException(final String msg, final Throwable cause)
    {
        super(msg, cause);
    }

    /**
     * Constructs a new <code>InvalidAuthenticationException</code> with the specified cause.
     *
     * @param cause the cause
     */
    public InvalidAuthenticationException(final Throwable cause)
    {
        super(cause);
    }

    /**
     * Creates a new instance of an <code>InvalidAuthenticationException</code> with a default detail message using the
     * name of the entity that failed to authenticate.
     *
     * @param name name of entity
     * @return new instance of <code>InvalidAuthenticationException</code>
     */
    public static InvalidAuthenticationException newInstanceWithName(final String name)
    {
        return new InvalidAuthenticationException("Account with name <" + name + "> failed to authenticate");
    }

    /**
     * Creates a new instance of an <code>InvalidAuthenticationException</code> with a default detail message using the
     * name of the entity that failed to authenticate, and a cause.
     *
     * @param name name of entity
     * @param cause the cause
     * @return new instance of <code>InvalidAuthenticationException</code>
     */
    public static InvalidAuthenticationException newInstanceWithName(final String name, final Throwable cause)
    {
        return new InvalidAuthenticationException("Account with name <" + name + "> failed to authenticate", cause);
    }

    public static InvalidAuthenticationException newInstanceWithNameAndDescriptionFromCause(final String name, final Throwable cause)
    {
        return new InvalidAuthenticationException("Account with name <" + name + "> failed to authenticate: "
                + StringEscapeUtils.escapeJava(cause.getMessage()));
    }
}
