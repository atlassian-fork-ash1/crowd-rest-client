/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.service.client;

import java.util.Properties;

/**
 * Will provide information about the location of the Crowd resource used to configure a Crowd
 * Client. 
 */
public interface ResourceLocator
{
    /**
     * Will return the location of the resource on the file system.
     * This will be in URI format similar to "file:/crowd/temp/crowd.properties"
     * @return location of the resource
     */
    String getResourceLocation();

    /**
     * The configured name of the resource
     * @return name of resource
     */
    String getResourceName();

    /**
     * The Properties present within the given resource.
     * @return Properties or null if no properties exist 
     */
    Properties getProperties();
}
