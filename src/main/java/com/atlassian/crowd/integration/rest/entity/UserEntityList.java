/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contains a list of <tt>UserEntity</tt>s.
 *
 * @since v2.1
 */
@XmlRootElement (name = "users")
@XmlAccessorType (XmlAccessType.FIELD)
public class UserEntityList implements Iterable<UserEntity>
{
    @XmlElements (@XmlElement (name = "user"))
    private final List<UserEntity> users;

    /**
     * JAXB requires a no-arg constructor.
     */
    private UserEntityList()
    {
        users = new ArrayList<UserEntity>();
    }

    public UserEntityList(final List<UserEntity> users)
    {
        this.users = new ArrayList<UserEntity>(users);
    }

    public int size()
    {
        return users.size();
    }

    public boolean isEmpty()
    {
        return users.isEmpty();
    }

    public UserEntity get(final int index)
    {
        return users.get(index);
    }

    public Iterator<UserEntity> iterator()
    {
        return users.iterator();
    }
}

