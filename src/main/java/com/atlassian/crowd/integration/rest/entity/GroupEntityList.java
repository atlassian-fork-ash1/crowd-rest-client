/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.entity;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Contains a list of <tt>GroupEntity</tt>s.
 *
 * @since v2.1
 */
@XmlRootElement (name = "groups")
@XmlAccessorType (XmlAccessType.FIELD)
public class GroupEntityList implements Iterable<GroupEntity>
{
    @XmlElements (@XmlElement (name = "group"))
    private final List<GroupEntity> groups;

    /**
     * JAXB requires a no-arg constructor.
     */
    private GroupEntityList()
    {
        groups = new ArrayList<GroupEntity>();
    }

    public GroupEntityList(final List<GroupEntity> groups)
    {
        this.groups = new ArrayList<GroupEntity>(groups);
    }

    public int size()
    {
        return groups.size();
    }

    public boolean isEmpty()
    {
        return groups.isEmpty();
    }

    public GroupEntity get(final int index)
    {
        return groups.get(index);
    }

    public Iterator<GroupEntity> iterator()
    {
        return groups.iterator();
    }
}
