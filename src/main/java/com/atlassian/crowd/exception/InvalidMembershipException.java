/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.exception;

/**
 * Thrown when a user tries to create a Nested Group membership that is not valid.
 * Reasons could include:
 * <ul>
 * <li>The parent group and child group are the same.</li>
 * <li>The parent group and child group are of different types (one is a Group and the other is a Role).</li>
 * <li>The new membership would cause a circular reference in the given application.</li>
 * </ul>
 */
public class InvalidMembershipException extends CrowdException
{
    public InvalidMembershipException(final String message)
    {
        super(message);
    }

    public InvalidMembershipException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public InvalidMembershipException(final Throwable cause)
    {
        super(cause);
    }
}
