/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.entity;

import com.atlassian.crowd.model.authentication.ValidationFactor;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * List of ValidationFactors.
 */
@XmlRootElement (name = "validation-factors")
@XmlAccessorType (XmlAccessType.FIELD)
public class ValidationFactorEntityList
{
    @XmlElements (@XmlElement (name="validation-factor", type= ValidationFactorEntity.class))
    private final List<ValidationFactorEntity> validationFactors;

    private ValidationFactorEntityList()
    {
        validationFactors = new ArrayList<ValidationFactorEntity>();
    }

    public ValidationFactorEntityList(final List<ValidationFactorEntity> validationFactors)
    {
        this.validationFactors = validationFactors;
    }

    public List<ValidationFactorEntity> getValidationFactors()
    {
        return validationFactors;
    }

    public static ValidationFactorEntityList newInstance(List<ValidationFactor> validationFactors)
    {
        List<ValidationFactorEntity> validationFactorEntities = new ArrayList<ValidationFactorEntity>();
        for (ValidationFactor vf : validationFactors)
        {
            validationFactorEntities.add(ValidationFactorEntity.newInstance(vf));
        }
        return new ValidationFactorEntityList(validationFactorEntities);
    }
}
