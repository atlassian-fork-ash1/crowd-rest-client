/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.exception;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.PasswordConstraint;
import com.google.common.collect.ImmutableList;


/**
 * Thrown when the supplied credential is not valid.
 */
public class InvalidCredentialException extends CrowdException
{
    @Nullable
    private final Collection<PasswordConstraint> violatedConstraints;

    @Nullable
    private final String policyDescription;

    public InvalidCredentialException()
    {
        this.policyDescription = null;
        this.violatedConstraints = null;
    }

    public InvalidCredentialException(String message)
    {
        super(message);
        this.policyDescription = null;
        this.violatedConstraints = null;
    }

    /**
     * Use this constructor when you can identify a specific policy that has been violated.
     * If the policy is not known, use one of the other constructors.
     *
     * @param genericMessage a general message describing how this exception happened
     * @param policyDescription a message describing the policy that has been violated
     * @param violatedConstraints a list of which constraints were violated to cause the failure
     */
    public InvalidCredentialException(String genericMessage, @Nullable String policyDescription, Collection<PasswordConstraint> violatedConstraints)
    {
        super(policyDescription == null ? genericMessage : genericMessage + ": " + policyDescription);
        this.policyDescription = policyDescription;
        this.violatedConstraints = ImmutableList.copyOf(violatedConstraints);
    }

    public InvalidCredentialException(String message, Throwable cause)
    {
        super(message, cause);
        this.policyDescription = null;
        this.violatedConstraints = null;
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public InvalidCredentialException(Throwable throwable)
    {
        super(throwable);
        this.policyDescription = null;
        this.violatedConstraints = null;
    }

    /**
     * @return a description of the policy that has been violated, if available. If such description is
     * not available, this method returns null. In that case, refer to {@link #getMessage()} for a general
     * description of the exception.
     */
    @Nullable
    public String getPolicyDescription()
    {
        return policyDescription;
    }

    /**
     * @return a list of all the constraints that were violated, or null if the policy that has been violated
     * is not known
     */
    @Nullable
    public Collection<PasswordConstraint> getViolatedConstraints()
    {
        return violatedConstraints;
    }

}
