/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.entity;

import javax.xml.bind.annotation.*;
import java.util.Collection;

/**
 * Represents a multi-valued attribute.
 *
 * @since v2.1
 */
@XmlRootElement (name = "attribute")
@XmlAccessorType(XmlAccessType.FIELD)
public class MultiValuedAttributeEntity
{
    @XmlAttribute (name = "name")
    private final String name;

    @XmlElementWrapper(name = "values")
    @XmlElements(@XmlElement(name = "value"))
    private final Collection<String> values;

    /**
     * JAXB requires a no-arg constructor.
     */
    private MultiValuedAttributeEntity()
    {
        name = null;
        values = null;
    }

    /**
     * Constructs a new MultiValuedAttributeEntity.
     *
     * @param name name of the attribute
     * @param values values of the attribute
     */
    public MultiValuedAttributeEntity(final String name, final Collection<String> values)
    {
        this.name = name;
        this.values = values;
    }

    /**
     * @return name of the attribute
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return values of the attribute
     */
    public Collection<String> getValues()
    {
        return values;
    }
}
