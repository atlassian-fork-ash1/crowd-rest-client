/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.model.event;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.user.User;

import java.util.Map;
import java.util.Set;

public class UserEvent extends AbstractAttributeEvent
{
    private final User user;

    public UserEvent(Operation operation, Directory directory, User user, Map<String, Set<String>> storedAttributes, Set<String> deletedAttributes)
    {
        super(operation, directory, storedAttributes, deletedAttributes);
        this.user = user;
    }

    public User getUser()
    {
        return user;
    }

    @Override
    public String toString()
    {
        return "UserEvent{" +
               "operation=" + getOperation() +
               ",directory=" + getDirectory() +
               ",storedAttributes=" + getStoredAttributes() +
               ",deletedAttributes=" + getDeletedAttributes() +
               ",user=" + user +
               '}';
    }
}
